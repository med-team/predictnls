Summary: prediction and analysis of nuclear localization signals
Name: predictnls
Version: 1.0.18
Release: 2
License: GPL
Group: Applications/Science
URL: https://rostlab.informatik.tu-muenchen.de/owiki/index.php/PredictNLS
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Requires: pp-popularity-contest

%description
 PredictNLS predicts that your protein is nuclear or finds out whether your potential NLS is found in our database. 
 The program also compiles statistics on the number of nuclear/non-nuclear proteins in which your potential NLS is found. 
 Finally, proteins with similar NLS motifs are reported, and the experimental paper describing the particular NLS are given. 
 If no NLS is found, you can predict the subcellular localization of the protein using LOCtree. 

%prep
%setup -q

%build
%configure
make


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{_defaultdocdir}/%{name}/README
/usr/bin/*
%{_mandir}/*/*
%{_datadir}/%{name}/*


%changelog
* Tue Jul 5 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.18-2
- req pp-popcon
* Tue Jun 28 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.18-1
- new upstream
* Wed Jun 22 2011 Guy Yachdav <gyachdav@rostlab.org> - 1.0.17-1
- Initial build.
